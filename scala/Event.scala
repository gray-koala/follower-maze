package follower_maze

sealed trait Event:
  def sequenceNr: Int
  def render: Array[Byte]

object Event:
  def parse(message: String): Event =
    val fields = message.split("\\|")
    val seqNr = fields.head.toInt
    fields(1) match
      case "F" => Event.Follow(seqNr, fields(2).toInt, fields(3).toInt)
      case "U" => Event.Unfollow(seqNr, fields(2).toInt, fields(3).toInt)
      case "B" => Event.Broadcast(seqNr)
      case "P" => Event.PrivateMsg(seqNr, fields(2).toInt, fields(3).toInt)
      case "S" => Event.StatusUpdate(seqNr, fields(2).toInt)

  final case class Follow(sequenceNr: Int, fromUserId: Int, toUserId: Int) extends Event:
    def render: Array[Byte] = s"$sequenceNr|F|$fromUserId|$toUserId\n".getBytes

  final case class Unfollow(sequenceNr: Int, fromUserId: Int, toUserId: Int) extends Event:
    def render: Array[Byte] = s"$sequenceNr|U|$fromUserId|$toUserId\n".getBytes

  final case class Broadcast(sequenceNr: Int) extends Event:
    def render: Array[Byte] = s"$sequenceNr|B\n".getBytes

  final case class PrivateMsg(sequenceNr: Int, fromUserId: Int, toUserId: Int) extends Event:
    def render: Array[Byte] = s"$sequenceNr|P|$fromUserId|$toUserId\n".getBytes

  final case class StatusUpdate(sequenceNr: Int, fromUserId: Int) extends Event:
    def render: Array[Byte] = s"$sequenceNr|S|$fromUserId\n".getBytes
