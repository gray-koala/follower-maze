package follower_maze

import java.io.InputStream
import java.net.{ServerSocket, Socket}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait Connectable:
  val port: Int

class SingleSourceConnector(val port: Int) extends Connectable:
  val stream: Future[InputStream] = Future {
      val listener = new ServerSocket(port)
      val socket = listener.accept()
      socket.getInputStream
    }

abstract class OutConnector[T](val port: Int) extends Thread with Connectable:
  override def run: Unit =
    val listener = new ServerSocket(port)
    while (true)
      val socket = listener.accept()
      accepted(socket)
  protected def write(socket: Socket, bytes: Array[Byte]): Unit =
    val out = socket.getOutputStream
    out.write(bytes)
    out.flush
  protected def accepted(socket: Socket): Unit
  def push(content: T): Unit

class ClientConnector(port: Int) extends OutConnector[Seq[(Event, Followers)]](port):
  private var _clients = Map.empty[Int, Socket]

  protected def accepted(socket: Socket): Unit =
    import java.util.Scanner
    val in = new Scanner(socket.getInputStream())
    val id = in.nextInt
    _clients += id -> socket
    
  private def _push(id: Int, bytes: Array[Byte]): Unit =
    _clients.get(id) match
      case Some(socket) => write(socket, bytes)
      case None => // ignore

  def push(content: Seq[(Event, Followers)]): Unit =
    import scala.collection.parallel.CollectionConverters._
    content.foreach { case (event, followers) =>
      val raw = event.render
      event match
        case _event: (Event.Follow | Event.PrivateMsg) =>
          val toId = _event match
            case Event.Follow(_, _, id) => id
            case Event.PrivateMsg(_, _, id) => id
          _push(toId, raw)
        case Event.StatusUpdate(_, fromId) =>
          followers.get(fromId) match
            case Some(ids) => ids.par.foreach(_push(_, raw))
            case None => // ignore
        case Event.Broadcast(_) => _clients.values.par.foreach(write(_, raw))
        case _ => // ignore
    }
