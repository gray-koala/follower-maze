package follower_maze

trait Flow[In, Out] extends (In => Out)

object Flow:
  val reorder = new Flow[Event, Seq[Event]] {
    import scala.collection.immutable.SortedSet
    var buffer = SortedSet.empty[Event](_.sequenceNr compare _.sequenceNr)
    var expectedNr = 1

    def apply(event: Event): Seq[Event] =
      buffer += event
      val out = buffer.toSeq
                      .zip(LazyList from expectedNr)
                      .takeWhile(_.sequenceNr == _)
                      .map(_(0))
      buffer = buffer -- out
      expectedNr = if out.nonEmpty then out.last.sequenceNr + 1 else expectedNr
      out
  }

  val associate = new Flow[Seq[Event], Seq[(Event, Followers)]] {
    var followers: Followers = Map.empty

    def apply(events: Seq[Event]): Seq[(Event, Followers)] =
      val init = (followers, Seq.empty[(Event, Followers)])
      val (updated, out) = events.foldLeft(init) { case ((map, out), event) =>
          val ids = (id: Int) => map.getOrElse(id, Set.empty[Int])
          val updated = event match
            case Event.Follow(_, from, to) => map.updated(to, ids(to) + from)
            case Event.Unfollow(_, from, to) => map.updated(to, ids(to) - from)
            case _ => map
          (updated, out :+ (event, updated))
        }
      followers = updated
      out
  }
