package follower_maze

import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.BufferedSource

object Server:
  @main def start: Unit =
    val downstream = new ClientConnector(port = 9099)
    downstream.start
    new SingleSourceConnector(port = 9090).stream.map { input =>
        val pipeline = (message: String) =>
          import scala.util.chaining._
          message.pipe(Event.parse)
                 .pipe(Flow.reorder)
                 .pipe(Flow.associate)
                 .pipe(downstream.push)
        new BufferedSource(input).getLines.foreach(pipeline)
      }
